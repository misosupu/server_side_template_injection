import datetime
from flask_sqlalchemy import SQLAlchemy
#from SQLAlchemy import create_engine
#from SQLAlchemy.orm import sessionmaker
from table_def import *
 
engine = create_engine('sqlite:///app.db', echo=True)
 
# create a Session
Session = sessionmaker(bind=engine)
session = Session()
 
user = User("admin", "password")
session.add(user)
 
user = User("pete", "pete")
session.add(user)
 
user = User("mary", "mary")
session.add(user)
 
user = User("joe", "joe")
session.add(user)

# commit the record the database
session.commit()
 
session.commit()