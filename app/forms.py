from flask_wtf import Form
from wtforms import StringField, BooleanField, TextField, PasswordField, validators
from wtforms.validators import DataRequired


class LoginForm(Form):
    username = TextField('Username', [validators.Required(), validators.Length(min=3, max=25)])
    password = PasswordField('Password', [validators.Required(), validators.Length(min=3, max=200)])


class RegistrationForm(Form):
    name = TextField('Your Name', [validators.Required(), validators.Length(min=3, max=30)])
    email = TextField('E-mail', [validators.Required()])
    username = TextField('Username', [validators.Required(), validators.Length(min=3, max=25)])
    password = PasswordField('Password', [validators.Required(), validators.Length(min=3, max=100)])
