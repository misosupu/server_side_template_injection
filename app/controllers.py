def register_user():
    form = RegistrationForm(request.form)

    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = form.password.data
        
        user_id = User.query.filter_by(username=username).first()

        if user_id is not None:
            flash("That username is already taken, please use another one.")
            return render_template('register.html', title='User Registration', form=form)
        else:
            # create a new user
            user = User(form.email, form.username, form.password)
            app.db.session.add(user)
            app.db.session.commit()
