from flask import render_template, flash, redirect, current_app, request, url_for, session, render_template_string # for SSTI vulnerability purposes
from app import app, db, mail
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
from flask_httpauth import HTTPBasicAuth
from jinja2 import Template
from flask_mail import Message

import logging

from .forms import LoginForm, RegistrationForm
from .models import User
from config import ADMINS


# app.app_context()
login_manager = LoginManager()
# login_manager.setup_app(current_app)
login_manager.setup_app(app)
login_manager.init_app(app)
auth = HTTPBasicAuth()


# needed to create a context for the login_manager
@login_manager.user_loader
def user_loader(user_id):
    """Given *user_id*, return the associated User object.

    :param unicode user_id: user_id (email) user to retrieve
    """
    return User.query.get(user_id)


@app.errorhandler(404)
def page_not_found(e):
    # return render_template('404.html'), 404
    template = '''{%% extends "base.html" %%}
                {%% block content %%}
            <div class="center-content error">
                <img src="static/images/dog-detective.png" height="200" width="150" align="middle"><br><br>
                <h1 id="secret-message">Oops! That page doesn't exist.</h1>
                <h3>%s</h3>
            </div>
        {%% endblock %%}''' % (request.url)

    return render_template_string(template), 404


# @app.route('/')
@app.route('/register', methods=['GET', 'POST'])
def register():
    # return render_template('register.html', title='Test Mest')
    form = RegistrationForm(request.form)
    error = None

    if request.method == 'POST':
        print("Form info: {}", form)

        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = form.password.data

        print("Name: {}, email: {}, username: {}, password: {}".format(name, email, username, password))

        user_id = User.query.filter_by(username=username).first()

        print("User id: {}".format(user_id))
        
        if(form.validate()):
            print("Form.validate() returned true")

        if user_id is not None:
            flash("That username is already taken, please choose another one.")
            return render_template('register.html', title='User Registration', form=form, messages=error)
        elif user_id is None: #  and form.validate_on_submit()
            # create a new user
            user = User(username, password, email)
            db.session.add(user)
            db.session.commit()

            # send confirmation email
            confirmation_message = Message("Thank you for registering to our wonderful website",
                  sender=ADMINS[0],
                  recipients=[user.email])

            print("Send mail from: {}, to: {}".format(ADMINS[0], user.email))

            # confirmation_message.html = Template('<p>Thanks, {{ name }}, for registering<br>Sincerely,<br>Admin</p>').render(name = name)

            message_template = '''<p>Thanks, %s, for registering<br>Sincerely,<br>Admin</p>''' % (name)
            confirmation_message.html = render_template_string(message_template)

            print(confirmation_message.html)
            mail.send(confirmation_message)

            flash('You\'ve succesfully created a profile! You can login now.')
            error = 'You\'ve succesfully created a profile! You can login now.'

            return render_template('login.html', title='Login Page', messages=error)

        # elif user_id is None and not form.validate_on_submit():
        #     flash("Incorrect data")
        #     return render_template('register.html', title='Registration Page', form=form)

    if request.method == 'GET':
        return render_template('register.html', title='Registration Page', form=form)


@app.route('/')
def home():
    return redirect(url_for('login'))

# home page
@app.route('/user/<username>')
# @app.route('/')
def homepage(username):
    user = User.query.filter_by(username=username).first()

    if user is None:
        flash('User %s not found', username)
        return redirect(url_for('index'))

    return render_template('user.html', user=username)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    messages = []
    
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('secret'))

    if request.method == 'POST':
        user = User.query.filter_by(username=request.form.get('username').lower()).first()
        # user = app.db.users.filter_by(username='joe')

        if user and user.password == request.form.get('password'):
            try:
                login_user(user)
                flash('You were logged in.')
                messages.append('You were logged in.')

                app.logger.debug('Logged in user %s', user.username)
                return redirect(url_for('secret'))
            except:
                messages.append('Invalid username or password.')

        flash(messages)
    return render_template('login.html', form=form, messages=messages)


@app.route('/logout', methods=['GET'])
# @login_required
def logout():
    # user = current_user
    # app.db.session.add(user)
    # app.db.session.commit()
    logout_user()
    # user.authenticated = False

    session.clear()

    flash('You were logged out.')

    return redirect(url_for('login'))


@app.route('/secret', methods=['GET'])
@login_required
def secret():
    # return 'This is a secret page. You are logged in as {} <br /><a href="/logout">Logout</a>'.format(current_user.username)
    return render_template('secret.html', user=current_user)


@app.route('/about', methods=['GET'])
def about():
    return render_template('about.html')